<div align="center">
  <h1><strong>Commercial Detection and Replacement</strong></h1>
  <h3>
    :movie_camera: Detecting Commercials and Replacing them with alternate content :clapper:
    <br>
    [BeagleBoard.org ](https://www.beagleboard.org/) - [GSoC 2024](https://summerofcode.withgoogle.com/programs/2024)
  </h3>
</div>

<hr>
<details>
<summary>Table of Contents</summary>

- [Motivation behind the project](#motivation-behind-the-project)
- [BeagleBone AI-64](#beaglebone-ai-64)
- [File Structure](#file-structure)
- [Connections](#connections)
- [Why BeagleBone AI-64?](#why-beaglebone-ai-64?)
- [Commercial Detection Model](#commercial-detection-model)
- [Benefits of this Project](#benefits-of-this-project)
- [Important Links](#important-links)
- [Contributors](#contributors)
- [Mentors](#mentors)
- [Acknowledgements](#acknowledgements)
- [Contribution Guidelines](#contribution-guidelines)

</details>
<hr>

## Motivation behind the project

The primary motivation behind this project is to enhance the media consumption experience by providing users with control over the content they see during commercial breaks. Advertisements can often be disruptive, irrelevant, or inappropriate for the viewing context.

For instance:

- Say, we are watching a sports match or a comedy movie and suddenly encounter an ad for a horror movie or a crime drama, this can be jarring and can negatively impact the viewing experience.

<center>
<img src="./assets/scary.png" width="500px"/>
</center>

- Also, Certain advertisements may contain content that is disturbing or unsuitable for all audiences, especially when watching with children.

By developing a system that detects and replaces commercials in real-time, this project aims to ensure that viewers can enjoy a seamless and pleasant viewing experience, tailored to their preferences. Also it will serve as an educational resource on integrating AI and machine learning capabilities into embedded systems.

## BeagleBone AI-64

<center>
<img src="./assets/BegleBoneAi-64.png" width="700px" height="500px" />
<p><a href="https://docs.beagleboard.org/boards/beaglebone/ai-64/ch04.html" target="_blank">Source</a></p>
</center>
BeagleBone® AI-64 brings a complete system for developing artificial intelligence (AI) and machine learning solutions with the convenience and expandability of the BeagleBone® platform and the peripherals on board to get started right away learning and building applications. The hardware supports real-time H.264 and H.265 (HEVC) video encoding and decoding. Typical performance metrics indicate the capability to handle 1080p streams at 60 frames per second (fps) or multiple 720p streams simultaneously.

### Connections

<center>
<img src="./assets/BBAi64_Connections.png" width="500px"/>
<p><a href="https://docs.beagleboard.org/boards/beaglebone/ai-64/ch03.html" target="_blank">Source</a></p>
</center>

For the project, I'll take input of media from an HDMI source and, after processing it, the output will be displayed on a monitor using a miniDP to HDMI cable.

## Why BeagleBone AI-64?

Using BeagleBone AI-64 hardware is a key aspect of this project, offering several benefits:

- The BeagleBone AI-64 is powered by the Texas Instruments TDA4VM processor, which includes multiple high-performance Arm Cortex-A72 cores and Cortex-R5F cores. This provides significant processing power for running complex inferencing algorithms.
- The TDA4VM processor integrates a dedicated deep learning accelerator (DLA), which significantly boosts the performance of AI inferencing tasks. It can accelerate the inferencing of deep learning models by offloading and parallelizing computation-intensive operations, thus reducing the time required for each inference.
- The hardware’s architecture and support for various multimedia processing frameworks, such as GStreamer, OpenCV make it an ideal choice for developing and running the proposed system efficiently.

<center>
<img src="./assets/Comparison.png" width="700px" height="350px" />
<p><a href="https://www.ti.com/lit/an/spracz2/spracz2.pdf?ts=1716900138738&ref_url=https%253A%252F%252Fwww.google.com%252F" target="_blank">Source</a></p>
</center>

We can see from the above comparison that TDA4VM is up to 60% better in terms of FPS/TOPS efficiency. What this means is that 60% less TOPS are needed to run equivalent deep learning functions.

## Commercial Detection Model

Deep learning is a subset of machine learning that involves neural networks with many layers, known as deep neural networks. Each layer of the network extracts increasingly abstract features from the input data, enabling the system to understand and generate intricate representations.

For the project, I'll develop an audio-visual CNN model that combines Mel-spectrogram data from audio with frames extracted from videos as its input. To extract features, I'll utilize well-known CNN architectures like MobileNetV2, InceptionV3, and DenseNet169. Ultimately, I'll merge the features obtained from the audio and visual components and then conduct classification based on these merged features and then will train the model accordingly.

### File Structure 

```
.
├── Dockerfile
├── Model
│   ├── datasetCollectionAndFeatureExtraction
│   │   ├── README.md
│   │   ├── yt8m_dataset_CommercialAndNonCommercial_FeatureExtraction
│   │   │   └── yt8m_CommercialAndNonCommercial_FeatureExtraction.ipynb
│   │   └── yt8m_explore
│   │       └── Yt8m_explore.ipynb
│   ├── datasetPreProcessing
│   │   ├── generateUniformFeatures.ipynb
│   │   └── mergeAudioVisualFeatures.ipynb
│   ├── inferencing
│   │   └── tflite_model_inferencing.ipynb
│   └── model_training
│       ├── all_models
│       │   ├── CNNs_Model.ipynb
│       │   ├── model_train.ipynb
│       │   └── visualFeaturesOnlyCNNsModel_train.ipynb
│       └── model_train.ipynb
├── README.md
├── assets
│   ├── BBAi64_Connections.png
│   ├── BegleBoneAi-64.png
│   ├── Comparison.png
│   └── scary.png
├── bbai64
│   ├── compilation_Python
│   │   ├── Model
│   │   ├── artifacts
│   │   ├── cal_Data
│   │   │   ├── cal_1.npy
│   │   │   ├── cal_2.npy
│   │   │   ├── cal_3.npy
│   │   │   ├── cal_4.npy
│   │   │   ├── cal_5.npy
│   │   │   └── cal_6.npy
│   │   ├── compile.py
│   │   ├── config.json
│   │   ├── edgeai-tidl-tools-08_02_00_05.Dockerfile
│   │   ├── generateCaliData.ipynb
│   └── inferencing
│       ├── Model
│       ├── artifacts_folder
│       ├── environment.yml
│       ├── inferencing
│       │   └── tflite_model_inferencing.ipynb
│       └── test_data
├── ci_env
├── environment.yml
├── realTimeInferencing
│   ├── Model
│   ├── music
│   │   └── music.mp3
│   ├── pca
│   │   ├── eigenvals.npy
│   │   ├── eigenvecs.npy
│   │   └── mean.npy
│   ├── realTimeInferencing.py
│   └── test
│       ├── FeatureExtraction.py
│       ├── Vggish
│       ├── audiofeatureextraction.py
│       └── staticCheck.py
└── run_ci
```

## Benefits of this Project

- This project aims to improve both the media consumption experience for BeagleBoard hardware users and serve as an educational tool for integrating AI and machine learning into embedded systems.
- It offers valuable insights into deploying neural network models in resource-constrained environments, creating custom GStreamer plugins for multimedia processing, and applying machine learning in real-world scenarios to enhance digital media experiences.
- Moreover, it enhances the viewing experience by automatically detecting and replacing commercials with preferred content, ensuring uninterrupted entertainment.

## Important Links

- Project Description and Discussion **[Link](https://forum.beagleboard.org/t/enhanced-media-experience-with-ai-powered-commercial-detection-and-replacement/37358)**
- GSoC 2024 proposal **[Link](https://gsoc.beagleboard.io/proposals/commercial_detection_and_replacement.html)**
- Gitlab Repo **[Link](https://openbeagle.org/aryan_nanda/gsoc_2024-enhanced_media_experience_with_ai-powered_commercial_detection_and_replacement)**
- Intro to Project Youtube Video **[Link](https://www.youtube.com/watch?v=Kagg8JycOfo)**
- Blogs **[Link](https://aryannanda17.github.io/Blogs/)**

## Contributors

* [Aryan Nanda](https://openbeagle.org/aryan_nanda) - nandaaryan823@gmail.com  

## Mentors

- [Jason Kridner](https://openbeagle.org/jkridner) 
- [Deepak Khatri](https://openbeagle.org/lorforlinux)
- [Abhishek Kumar](https://theembeddedkitchen.net/personal-portfolio)

## Acknowledgements 
- [BeagleBoard.org](https://www.beagleboard.org/) 
- [GSoC 2024](https://summerofcode.withgoogle.com/programs/2024)


## Contribution Guidelines

Contributions are what make the open-source community such an amazing place to learn, inspire, and create. Any contributions you make are greatly appreciated.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request